#!/bin/bash


update_file() {
	set +x # remove option temporarily
	diff $1 $2 >/dev/null 2>&1
	ret=$?
	if [ $ret -ne 0 ]; then
		echo "Updating $2"
		cp $1 $2
		set -x
		return 0
	else
		echo "Skip $2"
		set -x
		return 1
	fi
}

extract_images() {
    if [ ! -d 'webapp/public/image' ]; then
        sudo -u isucon mkdir 'webapp/public/image'
        sudo -u isucon GOROOT=/home/isucon/.local/go GOPATH=/home/isucon/gocode go run tools/extract_images.go
    fi
}

if [ "$EUID" -ne 0 ]; then # check sudo
	echo "Please run as root"
	exit 1
fi

set -ex

cd `dirname $0`

rsync -av etc/nginx/ /etc/nginx/
rm -rf /var/log/nginx/main_access.log
service nginx restart

systemctl stop isu-ruby || true


(
    cd webapp/golang
    sudo -u isucon GOROOT=/home/isucon/.local/go GOPATH=/home/isucon/gocode make
)

systemctl restart isu-go

mysql -uroot <<< 'use isuconp; ALTER TABLE posts ADD INDEX created_at_idx(created_at)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE posts ADD INDEX user_created_idx(user_id, created_at)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE posts ADD INDEX user_delflg_created_idx(user_id, del_flg, created_at)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE posts ADD INDEX user_idx(user_id)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE comments ADD INDEX user_id_idx(user_id)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE comments ADD INDEX post_created_idx(post_id, created_at)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE users ADD INDEX account_del_idx(account_name, del_flg)' || true
mysql -uroot <<< 'use isuconp; ALTER TABLE users ADD INDEX del_idx(del_flg)' || true

mysql -uroot <<< "use isuconp; ALTER TABLE posts ADD COLUMN del_flg tinyint(1) NOT NULL DEFAULT '0'" || true

mysql -uroot <<< "use isuconp; ALTER TABLE posts ADD INDEX del_created_idx(del_flg, created_at)" || true


update_file etc/mysql/mysql.conf.d/mysqld.cnf /etc/mysql/mysql.conf.d/mysqld.cnf && service mysql restart
update_file etc/sysctl.conf /etc/sysctl.conf && sysctl -p
update_file etc/security/limits.conf /etc/security/limits.conf || true
extract_images

echo ok
