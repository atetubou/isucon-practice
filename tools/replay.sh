#!/bin/bash

URL_PREFIXES="/posts/ /posts /@ /image" # URLがこのプレフィックスで始まるものは一つの統計情報にまとめる。

FILE_PREFIX=requests
FILE="$FILE_PREFIX"_0.gz # default file
SPEED="100%"

#set -eux
set -e

usage_exit() {
	echo "Usage: $0 [-m MIDDLEWARE_OPTIONS] [-s SPEED] [-f FILE]" >&2
	echo "       MIDDLEWARE_OPTIONS: ./middle.rbに渡すオプション。(example: -m '--timeout 3')"
	echo "       SPEED: 何倍で再生するか(default: 100%)"
	echo "       FILE: goreplayで再生するファイル名(default: requests_0.gz)"
	echo 'Example: ./replay.sh -m "--timeout 3" -s 200%'
	exit 1
}

while [ "$#" -gt 0 ]
do
	case $1 in
		'-m' )
			OPTION=$2
			shift 2
			;;
		'-s' )
			SPEED=$2
			shift 2
			;;
		'-f' )
			FILE=$2
			shift 2
			;;
		'-h' )  usage_exit
			;;
		*)  echo "Unknown option: $1"
			usage_exit
			;;
	esac
done

shift $((OPTIND - 1))
echo "replay speed: $SPEED"
OPTION="$OPTION $URL_PREFIXES"
echo "options for middleware: $OPTION"
echo "replay file: $FILE"
time sudo goreplay --input-file "$FILE|$SPEED" --middleware "./middle.rb $OPTION" --output-http=http://localhost --output-http-track-response --output-http-response-buffer 10000000

