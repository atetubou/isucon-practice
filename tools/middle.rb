#!/usr/bin/env ruby
require 'http'
require 'zlib'

require 'fileutils'
DIFF_FILE_DIR = "./diff/"
FileUtils.mkdir_p(DIFF_FILE_DIR)

class MyTimeoutException < StandardError; end

def parse_http_response(response)
	parser = HTTP::Response::Parser.new
	parser << response
	headers = HTTP::Headers.coerce(parser.headers)

	if %w(deflate gzip x-gzip).include?(headers[:content_encoding])
		body = Zlib::Inflate.new(32 + Zlib::MAX_WBITS).inflate(parser.chunk)
	else
		body = parser.chunk
	end
	body ||= ""

	return { :headers => headers, :body => body, :status_code => parser.status_code }
end

def hamming_distance(a, b)
	cnt = 0
	len = [a.length, b.length].max
	len.times do |i|
		cnt += 1 if a[i] != b[i]
	end
	return len > 0 ? cnt * 1.0 / len : 0
end

def distance_of_strings(a, b)
	cnt = 0
	c = a.split("\n")
	d = b.split("\n")
	len = [c.length, d.length].max
	len.times do |i|
		if !c[i] || !d[i]
			cnt += 1
		else
			cnt += hamming_distance(c[i], d[i])
		end
	end
	return len > 0 ? cnt * 1.0 / len : 0
end



class PerformanceStats
	def initialize(prefixes = [])
		@prefixes = prefixes
		@url2count = Hash.new(0)
		@url2time = Hash.new(0.0)
		@url2data = Hash.new {|h, k| h[k] = Hash.new(0) }
	end

	def string_of(x)
		if x.class == Float then
			"%.4f" % x
		else
			x.to_s
		end
	end

	def get_url_of(req_1stline) # the first line of request header
		url = req_1stline.split(" ")[1]
		req = req_1stline.split(" ")[0]
		@prefixes.each do |s|
			if url[0..s.length-1] == s then
				url = s
				break
			end
		end
		"#{req.ljust(4)} #{url}"
	end

	def add(req_1stline, time, others = {})
		url = get_url_of(req_1stline)
		@url2count[url] += 1
		@url2time[url] += time.to_f
		others.each do |h, k|
			@url2data[h][url] += k
		end
	end

	def inspect()
		additional_stats = @url2data.keys
		table = []
		@url2count.keys.each do |url|
			line = [@url2time[url], @url2count[url], @url2time[url]/@url2count[url], url]
			additional_stats.each do |k|
				line.push(@url2data[k][url] / @url2count[url])
			end
			table.push line
		end
		table.sort! {|a, b| b <=> a}

		header = ["time", "count", "time/count", "URL"] + additional_stats
		table = [header] + table
		lengths = table.transpose.map {|col| col.map{|x| string_of(x).length }.max }

		table.map {|line|
			[lengths, line].transpose.map{|len, x| string_of(x).ljust(len)}.join("  ")
		}.join("\n")
	end
end

def start_middleware(url_prefixes, timeout: 10, verbose: false)
	stats = PerformanceStats.new(url_prefixes)
	id2info = Hash.new {|h, k| h[k] = {}}
	total_distance = 0
	total_latency = 0.0
	n_processed = 0
	start_time = Time.now
	while $stdin.gets
		puts $_
		decoded = $_.scan(/../).map {|x| x.to_i(16).chr}.join
		head, response = decoded.split("\n", 2)
		flag, id, time, latency = head.split(" ")
		response ||= ""
		#$stderr.puts "#{flag} #{id}"

		flag = flag.to_i
		id2info[id][:flag] ||= 0
		id2info[id][:flag] += flag
		case flag
		when 1
			request_summary = response.split("\n", 2)[0].chomp
			id2info[id][:req_1stline] = request_summary
			method, request_url, http_version = request_summary.split(" ")
			id2info[id][:request_summary] = "#{method} #{request_url}"
		when 2
			original_response = response
			h = parse_http_response(original_response)
			id2info[id][:original] = h
		when 3
			id2info[id][:elapsed_time] = latency.to_f * 1e-9
			replayed_response = response
			id2info[id][:replayed] = parse_http_response(replayed_response)
		else
			raise "Unknown type: #{flag.to_i}"
		end

		# 1,2,3が全て現れていたら最終的な処理をする
		h = id2info[id]
		if h[:flag] == 6
			score = distance_of_strings(h[:original][:body], h[:replayed][:body])
			if verbose && score > 0 then
				File.open("#{DIFF_FILE_DIR}/#{id}_old", "w") {|f| f.write h[:original][:body]}
				File.open("#{DIFF_FILE_DIR}/#{id}_new", "w") {|f| f.write h[:replayed][:body]}
			end
			$stderr.puts "#{h[:replayed][:status_code]} (previously #{h[:original][:status_code]}) #{h[:request_summary].to_s.ljust(20)} #{h[:elapsed_time].round(6).to_s.ljust(8)} sec, distance = #{score.round(3).to_s.ljust(8)}, id = #{id}" if verbose
			total_distance += score
			total_latency += h[:elapsed_time]
			n_processed += 1
			stats.add(h[:req_1stline], h[:elapsed_time], { "distance" => score })
		end

		elapsed_time = Time.now - start_time
		if elapsed_time > timeout then
			raise MyTimeoutException, "The predetermined time (#{timeout} sec) has passed."
		end
	end
rescue MyTimeoutException => e
	$stderr.puts e
	exit 1
ensure
	begin
		stats = <<-EOS
Average distance: #{total_distance / n_processed}
Total latency: #{total_latency}
Total processed: #{n_processed}
Average latency: #{total_latency / n_processed}
Elapsed time: #{(Time.now - start_time).round(3)}
#{stats.inspect}
		EOS

		$stderr.puts stats
		File.open("replayed_stat.txt", "w") {|f| f.write stats }
	rescue
	end
end


if __FILE__ == $0
	require 'optparse'
	opt = OptionParser.new

	options = { }
	opt.on('-t TIMEOUT', '--timeout TIMEOUT') {|v| options[:timeout] = v.to_i }
	opt.on('-v', '--verbose') {|v| options[:verbose] = true }

	opt.parse!(ARGV)
	url_prefixes = ARGV 
	start_middleware(url_prefixes, **options)
end


